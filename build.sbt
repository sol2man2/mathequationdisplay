name := "MathEquationDisplay"

version := "0.1"

scalaVersion := "2.9.2"

//sampleKeyC in ThisBuild := "C: in build.sbt scoped to ThisBuild"

//sampleKeyD := "D: in build.sbt"

libraryDependencies ++= Seq(
//	"org.apache.derby" % "derby" % "10.4.1.3" % "test",
	"com.novocode" % "junit-interface" % "0.9" % "test",
	"junit" % "junit" % "4.10" % "test",
	"org.scalatest" %% "scalatest" % "1.8" % "test",
	"org.specs2" %% "specs2" % "1.12",
	"org.specs2" %% "specs2-scalaz-core" % "6.0.1" % "test"
)

resolvers ++= Seq(
	"junit interface repo" at "https://repository.jboss.org/nexus/content/repositories/scala-tools-releases",
	"scala releases"  at "https://oss.sonatype.org/content/groups/scala-tools/",
	"all release" at "https://oss.sonatype.org/content/repositories/snapshots/"
)

